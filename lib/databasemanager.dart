import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
        // Construct a file path to copy database to
  Directory documentsDirectory = await getApplicationDocumentsDirectory();
  String path = join(documentsDirectory.path, "bimos.sqlite");

  // Only copy if the database doesn't exist
  if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound){
    // Load database from asset and copy
    ByteData data = await rootBundle.load(join('assets', 'bimos.sqlite'));
    
    List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

    // Save copied asset to documents
    await new File(path).writeAsBytes(bytes);
  }
  
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE IF NOT EXISTS route_var_stop(id INTEGER PRIMARY KEY, route_id INTEGER, route_var_id INTEGER, stop_id INTEGER, stop_order INTEGER, wt TEXT, distance INTEGER)");
  }


   dynamic getListStopId(String routeId,String routeVarId) async{
    String sqlQuery = "SELECT stop_id FROM route_var_stop where route_id=" + routeId +" and route_var_id="+routeVarId+" order by stop_order";
    var data = await db;
    return  data.rawQuery(sqlQuery);
  }

  dynamic getListStopOrder(String routeId,String routeVarId) async{
    String  sqlQuery = "SELECT stop_order FROM route_var_stop WHERE route_id =" + routeId +" and route_var_id= " + routeVarId +" Order by stop_order";
    var data = await db;
    return  data.rawQuery(sqlQuery);
  }

  dynamic getListDistance(String routeId,String routeVarId) async{
    String  sqlQuery = "SELECT distance FROM route_var_stop WHERE route_id =" + routeId +" and route_var_id= " + routeVarId +" Order by stop_order";
    var data = await db;
    return  data.rawQuery(sqlQuery);
  }

  dynamic getSetRouteId_RouteVarId() async {	  
	    	  String sqlQuery = "SELECT route_id,route_var_id FROM route_var_stop Order By route_id,route_var_id";
		      var data = await db;
          return  data.rawQuery(sqlQuery);
	      }
  dynamic getRouteId_RouteVarId(String stopId1, String stopId2) async {	  
	    	  int routeId,routeVarId;
	    	  String sqlQuery = "SELECT R.route_id,R.route_var_id FROM route_var_stop R "+
	    			  "inner join route_var_stop A on R.route_id=a.route_id and r.route_var_id=a.route_var_id "+
	    			  "where r.stop_id=" + stopId1 + " and a.stop_id= "+stopId2;
		      var data = await db;
         return  data.rawQuery(sqlQuery);
	      }

  dynamic getSetStopId() async {	  
	    	  String sqlQuery = "select id from stop order by id ";
		      var data = await db;
        return   data.rawQuery(sqlQuery);
	      }      
}

class RouteId_RouteVarId {
  
		 int route_id;
		 int route_var_id;

		 RouteId_RouteVarId(int route_id,int route_var_id){
			this.route_id = route_id;
			this.route_var_id = route_var_id;
		}

  @override
  bool operator ==(other) {
   return (this.route_id == other.route_id && this.route_var_id==other.route_var_id);
	} 
  int get hashCode {
    return  route_id + route_var_id;
  }
}