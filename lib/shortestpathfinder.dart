import 'package:shortestpathproject/databasemanager.dart';

class ShortestPath 
{ 
  static final DatabaseHelper databaseHelper= new DatabaseHelper();
  static final double  MAX = 1000000;
	// A utility function to find the vertex with minimum distance value, 
	// from the set of vertices not yet included in shortest path tree 

	int minDistance(List<double> dist, List<bool> sptSet) 
	{ 
		// Initialize min value 
		double  min = MAX ;
    int min_index=-1; 

		for (int v = 0; v < dist.length; v++) 
			if (sptSet[v] == false && dist[v] <= min) 
			{ 
				min = dist[v]; 
				min_index = v; 
			} 

		return min_index; 
	} 

	// A utility function to print the constructed distance array 
	

	// Funtion that implements Dijkstra's single source shortest path 
	// algorithm for a graph represented using adjacency matrix 
	// representation 
	void dijkstra( List<List<double>> graph , int src,int dest, var listStopId,  Map<int,int> mapStopIdOrder) 
	{ 
		int size = listStopId.length;
		List<double>  dist = new List<double>.filled(size, MAX, growable: true); 
              // The output array. dist[i] will hold 
								// the shortest distance from src to i 
		Map<int,int> mapParent = new Map();
		// initial mapParent
		for (int i=0;i<size;i++) {
		//	mapParent.put(listStopId[i], -1);
    mapParent[listStopId[i]]=-1;
		}

		// sptSet[i] will true if vertex i is included in shortest 
		// path tree or shortest distance from src to i is finalized 
		List<bool> sptSet = new List<bool>.filled(size, false, growable: true); 

		// Distance of source vertex from itself is always 0 
		dist[mapStopIdOrder[src]] = 0; 

		// Find shortest path for all vertices 
		for (int count = 0; count < size-1; count++) 
		{ 
			// Pick the minimum distance vertex from the set of vertices 
			// not yet processed. u is always equal to src in first 
			// iteration. 
			int u = minDistance(dist, sptSet); 
			 
			// Mark the picked vertex as processed 
			sptSet[u] = true; 

			// Update dist value of the adjacent vertices of the 
			// picked vertex. 
			for (int v = 0; v < size; v++) {
				if(listStopId[v] == dest)
			{		
					if(graph[listStopId[u]][listStopId[v]]!=0) {
					List<int> listOrderedStop = new List<int>();
					mapParent[v]=u;
					int after=dest;
					listOrderedStop.add(dest);
					
					while(after!=src) {	
						listOrderedStop.add(mapParent[after]);
						after = mapParent[after];
					}
				  print(listOrderedStop);
					
					double  sum_length=0;

					for (int i=listOrderedStop.length-1;i>0;i--) {
					//	 address = database.getAddress(Integer.toString(listOrderedStop.get(i)));
						 var  obj = databaseHelper.getRouteId_RouteVarId(listOrderedStop[i].toString(), listOrderedStop[i-1].toString());
						// RouteVarName_RouteNo objRouteVarName_RouteNo = database.getRouteVarName_RouteNumber(Integer.toString(obj.route_id), Integer.toString( obj.route_var_id));
					//	 System.out.println(address);
						print(listOrderedStop[i]);
						print(obj.route_id.toString() + " "+ obj.route_var_id.toString());
						 sum_length+=graph[listOrderedStop[i]][listOrderedStop[i-1]];
					}
//					address = database.getAddress(Integer.toString(listOrderedStop.get(0)));
				print(listOrderedStop[0]);					
				print("length of path "+ sum_length.toString());
				return ;
					}
				}
				else {
				// Update dist[v] only if is not in sptSet, there is an 
				// edge from u to v, and total weight of path from src to 
				// v through u is smaller than current value of dist[v] 
				if (!sptSet[v] && graph[u][v]!=0 && 
						dist[u] != MAX && 
						dist[u]+graph[u][v] < dist[v]) 
				{ 
					dist[v] = dist[u] + graph[u][v]; 
					mapParent[v]=u;
				}
				}
			}
		} 
	
	} 

	// Driver method 
	void findShortTestPath() async
	{ 
	 // List<int> listStopOrder = new List<int>();
		  var listStopId = new List();
      var listDistance = new List();
      var listStopOrder = new List();
      List<List<double>> graph = new List<List<double>>.filled(10000, new List<double>.filled(10000, 0, growable: true) , growable: true); 

   //   var a = await databaseHelper.getListStopId(1.toString(),1.toString());
    RouteId_RouteVarId obj1 = new RouteId_RouteVarId(1,1);
    RouteId_RouteVarId obj2 = new RouteId_RouteVarId(1,1);
    bool a =(obj1==obj2);

      var temSetRouteId_RouteVarId  = await databaseHelper.getSetRouteId_RouteVarId();
      Set<RouteId_RouteVarId> setRouteId_RouteVarId  = new  Set<RouteId_RouteVarId>();
      for(int i=0;i<temSetRouteId_RouteVarId.length-1;i++){
        int routeId = temSetRouteId_RouteVarId[i]["route_id"];
        int routeVarId = temSetRouteId_RouteVarId[i]["route_var_id"];
        RouteId_RouteVarId obj = new RouteId_RouteVarId(routeId,routeVarId);
        setRouteId_RouteVarId .add(obj);
      }
      List<RouteId_RouteVarId> listRouteId_RouteVarId =  new  List<RouteId_RouteVarId>.of(setRouteId_RouteVarId);

      for(int i=0;i<setRouteId_RouteVarId.length;i++) {
			  // get ListStopId order by RouteId, RouteVarId
			  var   subListStopId = await databaseHelper.getListStopId(listRouteId_RouteVarId[i].route_id.toString(),listRouteId_RouteVarId[i].route_var_id.toString());
   //     subListStopId.toList();
        subListStopId = subListStopId.map((index) => index.row[0]).toList();
        listStopId.addAll(subListStopId);
       
			  
			  // get ListDistance order by RouteId, RouteVarId
			  var  subListDistance  = await databaseHelper.getListDistance(listRouteId_RouteVarId[i].route_id.toString(),listRouteId_RouteVarId[i].route_var_id.toString());
			   subListDistance =subListDistance.map((index) => index.row[0]).toList();
        listDistance.addAll(subListDistance);
			  
			  // get ListOrder order by RouteId, RouteVarId
			  var  subListOrder  = await databaseHelper.getListStopOrder(listRouteId_RouteVarId[i].route_id.toString(),listRouteId_RouteVarId[i].route_var_id.toString());
			  subListOrder = subListOrder.map((index) => index.row[0]).toList();
        listStopOrder.addAll(subListOrder);
			  
		  // assign distance
		  // vd 33-79 =1;
			  for (int k=0;k<subListOrder.length-1;k++) {	  
				  double  sumDistance =0;
				  for (int j=k;j<subListOrder.length-1;j++) {
						 graph[subListStopId[k]][subListStopId[j]]= sumDistance;
						 sumDistance += subListDistance[j+1];
			  }  
		  }
    
		  var setStopId = await databaseHelper.getSetStopId();
      setStopId = setStopId.map((index) => index.row[0]).toList();
		  Map<int,int> mapStopIdOrder = new Map();
		  for (int i=0;i<setStopId.length;i++) {
			  mapStopIdOrder[setStopId[i]]=i;
		  }	
							
		ShortestPath t = new ShortestPath(); 
		t.dijkstra(graph, 33, 64,setStopId,mapStopIdOrder); 
	} 
} 
//This code is contributed by Aakash Hasija 

}